This is the accounting for DebConf20. It uses [ledger][], a command-line
double-entry accounting program.

[ledger]: https://www.ledger-cli.org/

## File description

Meta files
* `accounts.inc`: a list of all the accounts used
* `commodities.inc`: a description of the commodities used
* `forex.db`: exchange rates

Ledger files:
* `budget.ledger`: the conference budget
* `expenses.ledger`: all expenses should be tracked there
* `income.ledger`: all income should be tracked there
* `journal.ledger`: meta ledger file that includes other relevant files

Scripts:
* `wrapper`: a more sensible way to call `ledger`
* `ledger-fx-rates`: fetches exchange rates from the European Central Bank

## Invoices

All expenses should be backed by invoices and be kept in the `invoices`
directory. The invoices should:

* be classified in directories by accounts
* start by `yyyymmdd_`
* have a short but descriptive name

For example:

```
invoices
├── childcare
│   └── 20170812_childcare.pdf
│   └── 20170817_music_show.pdf
├── roomboard
│   └── accommodation
│       ├── 20170810_hotel_universel_first_deposit.jpg
│       ├── 20170816_hotel_balance.pdf
│       └── 20170821_red_cross_rental.pdf
└── videoteam
    ├── 20170614_kyles_ticket.pdf
    └── 20170901_DigitalOcean_invoice_152897007.pdf
```

## Wrapper script

The easiest way to use this and include currency conversion is to call
`./wrapper` instead of `ledger`, e.g.

    ./wrapper -X USD bal

The wrapper script passes a few sensible default command-line parameter and
updates the currency exchange rates via `ledger-fx-rates`.
