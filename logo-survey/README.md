Data to used a input for https://www.condorcet.vote/:

* candidates: list of candidates
* results.csv: contents for the "votes" field

Full results file: results.orig.csv.asc (encrypted; ask @terceiro for access if
you need).


Results: https://www.condorcet.vote/Vote/D20D3ED15B/

Winner: S

![DebConf20 by Jefferson Maier](debconf20-jeffmaier.svg)
